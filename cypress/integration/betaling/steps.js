import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";



// egenskap betaling, scenario 1
Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get("#fullName").type("line")
    cy.get("#address").type("beverly hills 5")
    cy.get("#postCode").type("4661")
    cy.get("#city").type("holmenkollen")
    cy.get("#creditCardNo").type("1235553857356793")
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('form').submit();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.contains("Din ordre er registrert") 
});

// egenskap betaling, scenario 2
Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get("#fullName").type("line")
    cy.get("#address").type("beverly hills 5")
    cy.get("#postCode").type("4661")
    cy.get("#city").type("holmenkollen")
    cy.get("#creditCardNo").type("1235553857793")
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('form').submit();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    cy.contains("Kredittkortnummeret må bestå av 16 siffer") 
});